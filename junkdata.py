import random
import requests
import time
import json

last = 50

for i in range(0, 9000):
    last += random.randint(-5, 5)
    time.sleep(0.5)

    reqjson = {
        "value": last,
        "name": '_err'
    }

    print(json.dumps(reqjson))

    requests.post('http://localhost:8080/api/data', json=reqjson)
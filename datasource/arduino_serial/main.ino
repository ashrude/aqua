#include <OneWire.h>
#include <DallasTemperature.h>

int delaytime = 5100;

//Level sensor
int Level_trig=8;
int Level_echo=9;
long Level_length;
long Level_time;
//#Level sensor

//Turbidity sensor
int Turbidity_input=A0;
int Turbidity_sensorValue;
//#Turbidity sensor

//Tempurature sensor
#define ONE_WIRE_BUS 7

OneWire onewire(ONE_WIRE_BUS);

DallasTemperature sensors(&onewire);
//#Tempurature sensor

void setup() {
  // put your setup code here, to run once:

//Level sensor
  pinMode(Level_trig,OUTPUT);
  pinMode(Level_echo,INPUT);
  Serial.begin(9600);
//#Level sensor
//Tempurature sensor
  sensors.begin();
//#Tempurature sensor
}


void loop() {
  // Put at least 500 milliseconds between each sensor


//Level sensor
  digitalWrite(Level_trig,LOW);
  delayMicroseconds(2);
  
  digitalWrite(Level_trig,HIGH);
  delayMicroseconds(10);
  digitalWrite(Level_trig,LOW);
  
  Level_time = pulseIn(Level_echo,HIGH);
  Level_length=Level_time*0.017;
  
  Serial.print("out:LEVEL:");
  Serial.println(Level_length);
//#Level sensor
  delay(delaytime);
//Turbidity sensor
  Turbidity_sensorValue=analogRead(Turbidity_input);
  //Turbidity_sensorValue=50;

  Serial.print("out:TURBIDITY:");
  Serial.println(Turbidity_sensorValue);
//#Turbidity sensor
  delay(delaytime);
//#Tempurature sensor
  sensors.requestTemperatures();
  Serial.print("out:TEMPURATURE:");
  Serial.println(sensors.getTempCByIndex(0));
  delay(delaytime);
}

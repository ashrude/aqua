# Importing Libraries
import serial
import time
import json
import requests


SerialPort="/dev/ttyACM0"
baudrate=9600
api = 'http://aqua.ashleyr.ca:2000/api'


arduino = serial.Serial(port=SerialPort, baudrate=baudrate, timeout=.1)

lastRoundErr = False
while True:
    out = {}


    try:
        data = str(arduino.readline())
    except:
        if lastRoundErr == False:
            out['STATUS'] = "err"
            print(out)
        lastRoundErr = True

        #tries to reconnect arduino
        try:
            arduino = serial.Serial(port=SerialPort, baudrate=baudrate, timeout=.1)
        except:
            pass    
    
    else:
        lastRoundErr = False
        if "out" in data:
            string = data.replace("b'out:", '')[0:-5]
            split = string.split(':')
            out = {
                "name": split[0],
                "value": split[1]
            }


            print(json.dumps(out))
            #log to file
            try:
                f = open('./log', 'a')
                
                f.write(str(time.time()) + ':' + split[0] + ':' + split[1] + '\n')
                f.close()
            except:
                pass
            #log to server
            try:
                requests.post(api + "/data", json=out, timeout=5)
            except:
                pass
            


    #send data to server

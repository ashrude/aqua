//window.api = `${window.origin}/api`
window.api = `http://aqua.ashleyr.ca:2000/api`

function sensorBlock(container, sensor, starttime, endtime, resolution, labels) {
    $(container)
    .append($(`<h1>${sensor.name}</h1>`))
    .append($(`<p>${sensor.description}</p>`))
    .append(`<p>id: ${sensor.id}</p>`)
    .append(`<p>graph length: ${((endtime - starttime)/1000)/60} minutes</p>`)
    .append(
        $(`<button>close</button>`)
        .addClass('closeButton')
        .click(() => {container.remove();})
    );
        
    let chartElm = $(`<canvas width="400" height="400"></canvas>`)
    $(container).append(chartElm);

    // get data

    // fill in missing data
    const duration = endtime - starttime;
    const pointLength = duration/resolution;
    const points = [];

    // get datapoints
    let i = 0;
    while (i < resolution) {
        points[i] = {
            starttime: starttime + pointLength*i,
            endtime: starttime + pointLength*(i+1)
        };

        i++;
    }

    // get data
    fetch(window.api + `/data?sensor=${sensor.id}&starttime=${starttime}&endtime=${endtime}`)
    .then(data => data.json())
    .then(json => {
        // get average value for each point
        //TODO: optimise this, the data is already sorted
        const data = [];
        for (let point of points) {
            let average;
            for (let datapoint of json) {
                if (datapoint.timestamp > point.starttime && datapoint.timestamp < point.endtime) {
                    if (!average) {
                        average = datapoint.value;
                    } else {
                        average = (average + datapoint.value)/2;
                    }
                }
            }
            point.value = average;
            data.push(point.value);
        }


        // create chart
        let chart = new Chart(chartElm, {
            type: 'line',
            data: {
                labels: labels,
                datasets: [{
                    label: "",
                    data: data,
                    fill: false,
                    borderColor: 'rgb(75, 192, 192)',
                    tension: 0.1
                }]
            }
        })
    });
}

function chartAtTimes(endtime, timeperiod, sensor) {
    //javascript epoch is measured in milliseconds
    let range = (bottom, top) => {
        let i = bottom;
        let out = []
        while (i < top) {
            out.push(String(i));
            i++;
        }
        return out;
    }
    const lengths = {
        hour: {
            length: 3600000,
            resolution: 12,
            labels: ['0', '5', '10', '15', '20', '25', '30', '35', '40', '45', '50', '55']
        },
        day: {
            length: 86400000,
            resolution: 24,
            labels: ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '12', '13', '14', '15', '16', '17', '18', '19', '20', '21', '22', '23']
        },
        week: {
            length: 604800000,
            resolution: 7,
            labels: ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday']
        },
        highreshour: {
            length: 3600000,
            resolution: 60,
            labels: range(0, 60)
        },
        highresday: {
            length: 86400000,
            resolution: 48,
            labels: range(0, 48)
        }

    }
    let container = $('<div>')
    .addClass('sensorblock');

    const sensors = $('<div>')
    .addClass('sensors')
    .append(container);

    $('body').append(sensors);
    sensorBlock(container, sensor, (endtime - lengths[timeperiod].length), endtime, lengths[timeperiod].resolution, lengths[timeperiod].labels);

}


window.addEventListener('load', () => {
    
    fetch(window.api + '/sensors')
    .then(res => res.json())
    .then( data => {
        let form = $('<form id="createGraph_form">')
        .append(`<h3>add sensor graph</h3>`)
        .append($(`<label for="sensorSelector">Sensor</label>`))
        .append(
            $(`<select id="sensorSelector" name="sensor"></select>`)
            .append(() => {
                const out = [];
                data.forEach((sensor) => {
                    out.push($(`<option value=${sensor.id}>${sensor.name}</option>`))
                });
                return out;
            })
        )
        .append($(`<label for="timeFrameSelector">Time Frame</label>`))
        .append(
            $(`<select id="timeFrameSelector" name="timeframe"></select>`)
            .append(() => {
                const out = [];
                ['hour', 'day', 'week', 'highreshour', 'highresday'].forEach((item) => {
                    out.push($(`<option value="${item}">${item}</option>`));
                });
                return out;
            })
        )
        .append(
            $(`<button type="button">Create Graph</button>`)
            //yes the function for getting all the form values will be an anonymous function
            .click(() => {
                // sensor
                const sensor = $('#sensorSelector')[0].value;

                // timeframe
                const timeframe = $('#timeFrameSelector')[0].value;

                const sensorobj = data.find(element => element.id === Number(sensor))
                

                chartAtTimes(Date.now(), timeframe, sensorobj);
            })
        );
        console.log(data);

        $('body').append(form);
        
        // add sensor delete form
        const sensorElm = $(`<select id="sensorSelector" name="sensor"></select>`).append(() => {
            const out = [];
            data.forEach((sensor) => {
                out.push($(`<option value=${sensor.id}>${sensor.name}</option>`))
            });
            return out;
        })

        $(`body`).append($(`<button>more options</button>`).click(() => {
            $('body')
            .append(
                $(`<div>`)
                .append(`<h3>delete sensor</h3>`)
                .append(
                    $(`<form>`)
                    .append($(`<label for="sensorSelector">Sensor</label>`))
                    .append(sensorElm)
                    .append(
                        $(`<button type="button">delete sensor</button>`)
                        .click(() => {
                            if (confirm("You're about to delete a sensor and all it's data. Are you sure?")) {
                                if (confirm("Absolutly positive?")) {
                                    let sensorId = sensorElm[0].value;
                                    fetch(window.api + `/sensors?id=${sensorId}`, {method: 'DELETE'});
    
                                } else {alert("action canceled")}
                            } else {alert("action canceled")}
    
                        })
                    )
                )
            )

            // add sensor add form
            let sensorAddForm = {
                "name": $(`<input id="addSensor_name" type="text">`),
                "description": $(`<input id="addSensor_description" type="text">`)
            }
        
            $('body')
            .append(
                $('<div>')
                .append(`<h3>add sensor</h3>`)
                .append(
                    $(`<form>`)
                    .append(`<label for="addSensor_name">name: </label>`)
                    .append(sensorAddForm['name'])
                    .append(`<label for="addSensor_description">description: </label>`)
                    .append(sensorAddForm['description'])
                    .append($(`<button type="button">add sensor</button>`)
                    .click(() => {
                        let name = sensorAddForm['name'][0].value;
                        let description = sensorAddForm['description'][0].value;
        
                        fetch(window.api + `/sensors`, {
                            method: 'POST',
                            headers: {'Content-Type': 'application/json'},
                            body: JSON.stringify({"name": name, "description": description})
                        })
                    }))
                )
            )
        }))
    });

});

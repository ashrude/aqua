const sqlite3 = require("sqlite3");
const express = require('express');
const cors = require('cors');

const fs = require('fs');

const app = express();

const host = 'localhost';
const port = process.env.PORT || 2000;
const dbPath = '/home/ashley/aqua_api/main.db';

// database functions
function initdb(db) {
	db.exec(`
		CREATE TABLE sensors (
			id INTEGER PRIMARY KEY AUTOINCREMENT,
			name VARCHAR(255),
			description VARCHAR(255)
		);
		CREATE TABLE data (
			timestamp INT64 NOT NULL,
			value INT NOT NULL,
			sensor INT NOT NULL,
			FOREIGN KEY (sensor) REFERENCES sensors(id)
		);
		INSERT INTO sensors (name)
		VALUES ('_err');
	`);
}
//#database functions

// connect database
var db;

function connectDb () {
	db = new sqlite3.Database(dbPath, sqlite3.OPEN_READWRITE, (err) => {
		if (err && err.code == "SQLITE_CANTOPEN") {
			fs.open(dbPath, 'w', (err, file) => {if (err) throw err;});
			connectDb();
			initdb(db);
			return;
		} else if (err) {
			console.log('err' + err.code);
			exit(1);
		}
	});
}
connectDb();

//initdb(db)
//#connect database

// http listener

app.use(cors());
app.use(express.json());
//app.use(express.static('static'))


app.get('/api/data', (req, res) => {
	//get data from database
	db.all(`
		SELECT timestamp, value FROM data
		WHERE timestamp > :starttime
		AND timestamp < :endtime
		AND sensor = :sensor;
	`,
	[req.query.starttime, req.query.endtime, req.query.sensor], (err, rows) => {
		if (err) {res.status(500).send("unknown error");}
		//return data
		res.json(rows);
	});
});
app.post('/api/data', (req, res) => {
	//add data to database
	console.log(req.body);
	if (!req.body.sensor) {	
		db.all(`
			SELECT id FROM sensors WHERE name=:name;
		`, [req.body.name], (err, rows) => {
			try{
				let id = rows[0].id;
				db.run(`
				INSERT INTO data (timestamp, value, sensor)
				VALUES (:timestamp, :value, :sensor);
			`, [Date.now(), req.body.value, id], (err) => {if (err) {console.log(err)}});
			} catch {
				res.status(400).send();
				console.log("sensor doesn't exist")
				return;
			}
		});
	} else {
		let id = req.body.sensor;
		db.run(`
		INSERT INTO data (timestamp, value, sensor)
		VALUES (:timestamp, :value, :sensor);
	`, [Date.now(), req.body.value, id], (err) => {if (err) {console.log(err)}});
	}
	


	res.status(200).send();
});



app.get('/api/sensors', (req, res) => {
	db.all(`
		SELECT * FROM sensors
	`, [], (err, rows) => {
		if (err) {res.status(500).send("unknown error");}

		res.json(rows)
	})
});
app.post('/api/sensors', (req, res) => {
	console.log(req.body)
	db.run(`
		INSERT INTO sensors (name, description)
		VALUES (:name, :description)
	`, [req.body.name, req.body.description]);
	res.status(200).send();
});
app.delete('/api/sensors', (req, res) => {
	db.run(`
		DELETE FROM data WHERE sensor=:sensor;
	`, [req.query.id], (err) => {
		if (err) {console.log(err)}

		db.run(`
		DELETE FROM sensors WHERE id=:id;
		`, [req.query.id]);
	});
})

app.listen(port);
//#http listener
